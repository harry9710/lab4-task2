/*Write a programme, that generates a List of 10 000 integers (create a for loop and add 10 000 a generated integer with the code below)
	+ Start measuring the time
	+ add another 1000 to it in a for loop using the random generator (add it in the middle of the list, or at the beginning, not at the end!)
	+ end measuring time
	+ Do this separately for ArrayList and Linked List */

import java.util.*;

public class Main {
	public static void main(String[] args) {
		Random generator = new Random();
		int a = generator.nextInt(10) + 1;
		
		//---------ARRAYLIST PART---------//
		List<Integer> list1 = new ArrayList<>();
		for (int i = 0; i < 10000; i++) {
			list1.add(a);
		}
		//start measuring time
		long startTime = System.currentTimeMillis();
		//add another 1000 integers at the beginning of the list
		for (int i = 0; i < 1000; i++) {
			list1.add(0, a);
		}
		//end measuring time
		long estimatedTime = System.currentTimeMillis() - startTime;
		System.out.println(estimatedTime);
		
		//---------LINKEDLIST PART---------//
		LinkedList<Integer> list2 = new LinkedList<>();
		for (int i = 0; i < 10000; i++) {
			list2.add(a);
		}
		//start measuring time
		long startTime2 = System.currentTimeMillis();
		//add another 1000 integers at the beginning of the list
		for (int i = 0; i < 1000; i++) {
			list2.add(0, a);
		}
		//end measuring time
		long estimatedTime2 = System.currentTimeMillis() - startTime2;
		System.out.println(estimatedTime2);
		
	}
}
